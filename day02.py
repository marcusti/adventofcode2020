import re
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]


regex = r'([0-9]+)-([0-9]+) ([a-z]+): ([a-z]+)'
test_data = ['1-3 a: abcde', '1-3 b: cdefg', '2-9 c: ccccccccc']
data = read_data('day02.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data), 2)
        self.assertEqual(part1(data), 418)

    def test_part2(self):
        self.assertEqual(part2(test_data), 1)
        self.assertEqual(part2(data), 616)


def part1(passwords):
    valid = 0
    for password in passwords:
        min, max, char, pwd = parse(password)
        reg = '(?=%s)' % char
        num = len(re.findall(reg, pwd))
        if num >= int(min) and num <= int(max):
            valid += 1
    return valid


def part2(passwords):
    valid = 0
    for password in passwords:
        p1, p2, char, pwd = parse(password)
        if (pwd[p1-1] == char) ^ (pwd[p2-1] == char):
            valid += 1
    return valid


def parse(s):
    match = re.search(regex, s)
    if match:
        x = int(match.group(1))
        y = int(match.group(2))
        char = match.group(3)
        pwd = match.group(4)
        return (x, y, char, pwd)


if __name__ == '__main__':
    unittest.main()
