import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]


test_data = {
    'FBFBBFFRLR': 357,
    'BFFFBBFRRR': 567,
    'FFFBBBFRRR': 119,
    'BBFFBBFRLL': 820,
}
data = read_data('day05.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        for key, val in test_data.items():
            self.assertEqual(get_seat(key), val)
        self.assertEqual(part1(data), 813)

    def test_part2(self):
        self.assertEqual(part2(data), 612)


def part1(lines):
    return max([get_seat(x) for x in lines])


def part2(lines):
    seats = [get_seat(x) for x in lines]
    first, last = min(seats), max(seats)
    all = [i for i in range(first, last+1)]
    diff = set(all).difference(set(seats))
    return diff.pop()


def get_row(s):
    return parse(s[:7], 0, 127, 'B', 'F')


def get_col(s):
    return parse(s[7:10], 0, 7, 'R', 'L')


def parse(s, min, max, up, down):
    for c in s:
        diff = min+(max-min)//2
        if c == down:
            max = diff
        elif c == up:
            min = diff+1
    return min


def get_seat(s):
    return get_row(s)*8+get_col(s)


if __name__ == '__main__':
    unittest.main()
