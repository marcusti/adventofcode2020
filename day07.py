import re
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return f.readlines()


test_data = """light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."""

test_data_2 = '''shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.'''

data = read_data('day07.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data.split('\n')), 4)
        self.assertEqual(part1(data), 238)

    def test_part2(self):
        self.assertEqual(part2(test_data.split('\n')), 32)
        self.assertEqual(part2(test_data_2.split('\n')), 126)
        self.assertEqual(part2(data), 82930)


def part1(lines):
    colors = parse_colors(lines)
    bags = search_parent_bags('shiny gold', colors, [])
    return len(bags)


def part2(lines):
    colors = parse_colors(lines)
    return count_bags('shiny gold', colors)


def parse_colors(lines):
    colors = {}
    for line in lines:
        inner = re.findall(r'^\w+ \w+', line)[0]
        outer = re.findall(r'(\d) (\w+ \w+)', line)
        colors[inner] = outer
    return colors


def search_parent_bags(color, colors, bags=[]):
    for key, values in colors.items():
        for _, inner_color in values:
            if color == inner_color:
                bags.append(key)
                search_parent_bags(key, colors, bags)
    return set(bags)


def count_bags(color, colors):
    if not color in colors:
        return 0

    num_bags = 0
    for inner_num, inner_color in colors[color]:
        quantity = int(inner_num)
        num_bags += quantity*(count_bags(inner_color, colors)+1)
    return num_bags


if __name__ == '__main__':
    unittest.main()
