import re
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return f.read()


regex = '[a-z]{1}'

test_data = """abc

a
b
c

ab
ac

a
a
a
a

b"""
data = read_data('day06.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data), 11)
        self.assertEqual(part1(data), 6504)

    def test_part2(self):
        self.assertEqual(part2(test_data), 6)
        self.assertEqual(part2(data), 3351)


def part1(s):
    sum = 0
    groups = parse_groups(s)
    for group in groups:
        answers = set([x for x in group if re.match(regex, x)])
        sum += len(answers)
    return sum


def part2(s):
    sum = 0
    groups = parse_groups(s)
    for group in groups:
        people = group.split('\n')
        answers = [set(x) for x in people if re.match(regex, x)]
        sum += len(set.intersection(*answers))
    return sum


def parse_groups(s):
    return s.split('\n\n')


if __name__ == '__main__':
    unittest.main()
