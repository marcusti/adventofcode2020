import re
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]


regex = r'([a-z]{3}):(\S+)'
test_data = ['ecl:gry pid:860033327 eyr:2020 hcl:#fffffd',
             'byr:1937 iyr:2017 cid:147 hgt:183cm',
             '',
             'iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884',
             'hcl:#cfa07d byr:1929',
             '',
             'hcl:#ae17e1 iyr:2013',
             'eyr:2024',
             'ecl:brn pid:760753108 byr:1931',
             'hgt:179cm',
             '',
             'hcl:#cfa07d eyr:2025 pid:166559648',
             'iyr:2011 ecl:brn hgt:59in',
             ]
data = read_data('day04.txt')
required = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data), 2)
        self.assertEqual(part1(data), 260)

    def test_passports(self):
        self.assertFalse(Passport({'byr': '2003'}).is_valid_birth_year())
        self.assertFalse(Passport({'ecl': 'wat'}).is_valid_eye_color())
        self.assertFalse(Passport({'hcl': '#123abz'}).is_valid_hair_color())
        self.assertFalse(Passport({'hcl': '123abc'}).is_valid_hair_color())
        self.assertFalse(Passport({'hgt': '190'}).is_valid_height())
        self.assertFalse(Passport({'hgt': '190in'}).is_valid_height())
        self.assertFalse(Passport({'pid': '0123456789'}).is_valid_pid())

        self.assertTrue(Passport({'byr': '2002'}).is_valid_birth_year())
        self.assertTrue(Passport({'ecl': 'brn'}).is_valid_eye_color())
        self.assertTrue(Passport({'hcl': '#123abc'}).is_valid_hair_color())
        self.assertTrue(Passport({'hgt': '190cm'}).is_valid_height())
        self.assertTrue(Passport({'hgt': '60in'}).is_valid_height())
        self.assertTrue(Passport({'pid': '000000001'}).is_valid_pid())

    def test_part2(self):
        self.assertEqual(part2(data), 153)


def part1(lines):
    valid = 0
    passports = parse_passports(lines)
    for passport in passports:
        keys = [x[0] for x in re.findall(regex, passport)]
        if contains_all_required_keys(keys):
            valid += 1
    return valid


def part2(lines):
    valid = 0
    passports = parse_passports(lines)
    for passport in passports:
        p = Passport()
        keys = []
        for item in re.findall(regex, passport):
            p.add(item[0], item[1])
            keys.append(item[0])
        if contains_all_required_keys(keys) and p.is_valid():
            valid += 1
    return valid


def contains_all_required_keys(keys):
    return all(x in keys for x in required)


def parse_passports(lines):
    passports = []
    passport = ''
    for line in lines:
        line = line.strip()
        if line:
            passport = ' '.join((passport, line)).strip()
        else:
            passports.append(passport)
            passport = ''
    passports.append(' '.join((passport, line)).strip())
    return passports


class Passport:
    field_map = {}

    def __init__(self, map={}):
        self.field_map = map

    def add(self, key, value):
        self.field_map[key] = value

    def is_valid_birth_year(self):
        return self.is_valid_int('byr', 1920, 2002)

    def is_valid_issue_year(self):
        return self.is_valid_int('iyr', 2010, 2020)

    def is_valid_expiration_year(self):
        return self.is_valid_int('eyr', 2020, 2030)

    def is_valid_eye_color(self):
        ecl = self.field_map['ecl']
        return ecl in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

    def is_valid_hair_color(self):
        hcl = self.field_map['hcl']
        return len(re.findall(r'^#[a-f0-9]{6}$', hcl)) == 1

    def is_valid_pid(self):
        pid = self.field_map['pid']
        return len(re.findall(r'^[0-9]{9}$', pid)) == 1

    def is_valid_height(self):
        hgt = self.field_map['hgt']
        l = len(hgt)-2

        try:
            val = int(hgt[:l])
        except:
            return False

        if hgt.endswith('cm'):
            return val >= 150 and val <= 193
        elif hgt.endswith('in'):
            return val >= 59 and val <= 76
        else:
            return False

    def is_valid_int(self, key, min, max):
        if key not in self.field_map:
            return False
        val = int(self.field_map[key])
        return val >= min and val <= max

    def is_valid(self):
        return self.is_valid_birth_year() \
            and self.is_valid_issue_year() \
            and self.is_valid_expiration_year() \
            and self.is_valid_height() \
            and self.is_valid_eye_color() \
            and self.is_valid_hair_color() \
            and self.is_valid_pid()

    def __str__(self):
        return '%s valid=%s' % (self.field_map, self.is_valid())


if __name__ == '__main__':
    unittest.main()
