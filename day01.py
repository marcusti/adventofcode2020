from itertools import combinations
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return [int(line) for line in f.readlines()]


test_data = [1721, 979, 366, 299, 675, 1456]
data = read_data('day01.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data), 514579)
        self.assertEqual(part1(data), 838624)

    def test_part2(self):
        self.assertEqual(part2(test_data), 241861950)
        self.assertEqual(part2(data), 52764180)


def part1(numbers):
    for c in combinations(numbers, 2):
        i, j = c[0], c[1]
        if i+j == 2020:
            return i*j
    return 0


def part2(numbers):
    for c in combinations(numbers, 3):
        i, j, k = c[0], c[1], c[2]
        if i+j+k == 2020:
            return i*j*k
    return 0


if __name__ == '__main__':
    unittest.main()
