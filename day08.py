import re
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return f.read()


test_data = """nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"""

data = read_data('day08.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data), 5)
        self.assertEqual(part1(data), 1797)

    def test_part2(self):
        self.assertEqual(part2(test_data), 8)
        self.assertEqual(part2(data), 1036)


def part1(instructions):
    return execute(instructions, fail_on_infinite_loop=False)


def part2(instructions):
    val = 0
    for f, r in [('nop', 'jmp'), ('jmp', 'nop')]:
        for m in re.finditer(f, instructions):
            c = instructions[:m.start()]+r+instructions[m.end():]
            try:
                val = execute(c)
            except InfiniteLoopException:
                pass
    return val


def execute(instructions, fail_on_infinite_loop=True):
    pos = val = 0
    positions = set()
    lines = instructions.splitlines()

    while pos < len(lines) and not pos in positions:
        positions.add(pos)
        cmd, args = lines[pos].split(' ')
        if cmd == 'nop':
            pos += 1
        elif cmd == 'acc':
            val += int(args)
            pos += 1
        elif cmd == 'jmp':
            pos += int(args)

        if fail_on_infinite_loop and pos in positions:
            raise InfiniteLoopException()

    return val


class InfiniteLoopException(Exception):
    pass


if __name__ == '__main__':
    unittest.main()
