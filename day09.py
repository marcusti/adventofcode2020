from itertools import combinations
import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return f.read()


test_data = """35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"""

data = read_data('day09.txt')


def lines_to_numbers(data):
    return [int(line) for line in data.splitlines()]


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(lines_to_numbers(test_data), 5), 127)
        self.assertEqual(part1(lines_to_numbers(data), 25), 20874512)

    def test_part2(self):
        self.assertEqual(part2(lines_to_numbers(test_data), 127), 62)
        self.assertEqual(part2(lines_to_numbers(data), 20874512), 3012420)


def part1(numbers, preamble):
    for i in range(preamble, len(numbers)):
        curr = numbers[i]
        prev = numbers[i-preamble:i]
        hit = False
        for n, m in combinations(prev, 2):
            if n != m and n+m == curr:
                hit = True
        if not hit:
            return curr


def part2(numbers, goal):
    for i in range(2, len(numbers)-1):
        for j in range(0, len(numbers)-i+1):
            sub = numbers[j:j+i]
            if sum(sub) == goal:
                return min(sub)+max(sub)
    return 0


if __name__ == '__main__':
    unittest.main()
