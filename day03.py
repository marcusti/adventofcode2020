import unittest


def read_data(filename):
    with open(filename, 'r') as f:
        return [line.strip() for line in f.readlines()]


test_data = ['..##.......',
             '#...#...#..',
             '.#....#..#.',
             '..#.#...#.#',
             '.#...##..#.',
             '..#.##.....',
             '.#.#.#....#',
             '.#........#',
             '#.##...#...',
             '#...##....#',
             '.#..#...#.#',
             ]
data = read_data('day03.txt')


class Test(unittest.TestCase):
    def test_part1(self):
        self.assertEqual(part1(test_data), 7)
        self.assertEqual(part1(data), 237)

    def test_part2(self):
        self.assertEqual(part2(test_data), 336)
        self.assertEqual(part2(data), 2106818610)


def part1(map):
    return traverse(map, 3, 1)


def part2(map):
    return traverse(map, 1, 1) * \
        traverse(map, 3, 1) * \
        traverse(map, 5, 1) * \
        traverse(map, 7, 1) * \
        traverse(map, 1, 2)


def traverse(map, right, down):
    trees = row = col = 0
    line_length = len(map[0])
    while row < len(map)-1:
        col = (col+right) % line_length
        row += down
        if map[row][col] == '#':
            trees += 1
    return trees


if __name__ == '__main__':
    unittest.main()
